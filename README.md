1. Put your steam API key in a file called "api-key.sh"
2. Run `docker build --rm=false -t ttt .` (the rm=false ensures that the intermediate steps are kept around so if you need to change something you dont need to download gmod again)
3. Setup the large addon files /gmod-addons/ (ds_217325128.gma, ds_217729758.gma, ds_217820667.gma) `aws s3 cp s3://ttt-maps/ . --recursive`
4. If you don't use /gmod-addons/ you need to edit ./run.sh
5. Call `./run.sh`
