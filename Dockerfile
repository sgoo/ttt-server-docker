FROM debian:wheezy

MAINTAINER Scott Goodhew "scott.goodhew0@gmail.com"

# ------------
# Prepare Gmod
# ------------

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install lib32gcc1 wget
RUN mkdir /steamcmd
WORKDIR /steamcmd
RUN wget http://media.steampowered.com/installer/steamcmd_linux.tar.gz
RUN tar -xvzf steamcmd_linux.tar.gz
RUN mkdir /gmod-base
RUN /steamcmd/steamcmd.sh +login anonymous +force_install_dir /gmod-base +app_update 232330 +app_update 4020 validate +quit

# ----------------
# Annoying lib fix
# ----------------

RUN mkdir /gmod-libs
WORKDIR /gmod-libs
RUN wget --no-check-certificate https://launchpadlibrarian.net/195509222/libc6_2.15-0ubuntu10.10_i386.deb
RUN dpkg -x libc6_2.15-0ubuntu10.10_i386.deb .
RUN cp lib/i386-linux-gnu/* /gmod-base/bin/
WORKDIR /
RUN rm -rf /gmod-libs
RUN cp /steamcmd/linux32/libstdc++.so.6 /gmod-base/bin/

RUN mkdir /root/.steam
RUN mkdir /root/.steam/sdk32/
RUN cp /gmod-base/bin/libsteam.so /root/.steam/sdk32

# ----------------------
# Setup Volume and Union
# ----------------------

RUN mkdir /gmod-volume
VOLUME /gmod-volume
RUN mkdir /gmod-union
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install udev unionfs-fuse 


# ---------------
# Setup Container
# ---------------

EXPOSE 27015/udp

VOLUME /gmod-base/garrysmod/addons

ENV MAXPLAYERS="16"
ENV MAP="gm_construct"
ENV API_KEY=""

ADD start-server.sh /start-server.sh

# ----------------
# Add config files
# ----------------
ADD server.cfg /gmod-base/garrysmod/cfg/server.cfg
ADD mount.cfg /gmod-base/garrysmod/cfg/mount.cfg
ADD mapcycle.txt /gmod-base/garrysmod/cfg/mapcycle.txt
RUN mkdir -p /gmod-base/garrysmod/data/mapvote/
ADD mapvote-config.txt /gmod-base/garrysmod/data/mapvote/config.txt

CMD ["/bin/sh", "/start-server.sh"]
